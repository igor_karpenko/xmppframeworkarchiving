#import <UIKit/UIKit.h>

#import "ArchivedMessage.h"
#import "XMPPConfiguration.h"
#import "XMPPMessage+Additions.h"
#import "XMPPUserProtocol.h"
#import "XMPPMessageArchivingOpenfire.h"
#import "XMPPRoomCoreDataStorageOpenfire.h"
#import "XMPPChatViewController.h"
#import "XMPPService.h"

FOUNDATION_EXPORT double XMPPFrameworkArchivingVersionNumber;
FOUNDATION_EXPORT const unsigned char XMPPFrameworkArchivingVersionString[];

