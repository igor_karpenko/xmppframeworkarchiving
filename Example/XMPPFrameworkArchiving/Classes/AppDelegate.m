//
//  AppDelegate.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 03/17/2016.
//  Copyright (c) 2016 Igor Karpenko. All rights reserved.
//
#import "SettingsManager.h"
#import "XMPPService.h"
#import "XMPPTestUser.h"

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	NSString *hostName = [SettingsManager sharedManager].hostName;
	if (hostName.length > 0 && [SettingsManager sharedManager].username.length > 0) {
		XMPPTestUser *user = [XMPPTestUser new];
		user.username = [SettingsManager sharedManager].username;
		user.password = [SettingsManager sharedManager].password;
		
		XMPPConfiguration *configuration = [XMPPConfiguration new];
		configuration.hostName = hostName;
		configuration.user = user;
		
		[XMPPService sharedInstance].configuration = configuration;
		[[XMPPService sharedInstance] setupStream];
		[[XMPPService sharedInstance] signIn];
	}
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
