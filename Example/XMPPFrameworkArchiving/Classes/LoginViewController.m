//
//  LoginViewController.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/21/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import "XMPPTestUser.h"
#import "SettingsManager.h"
#import "XMPPService.h"
#import "UIAlertView+Blocks.h"
#import "SettingsManager.h"
#import "LoginViewController.h"

@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *hostTextField;
@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;

- (IBAction)loginButtonTapped:(id)sender;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

	self.hostTextField.text = [SettingsManager sharedManager].hostName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action methods

- (IBAction)loginButtonTapped:(id)sender {
	if (_hostTextField.text.length == 0) {
		return;
	}
	
	NSString *hostName = _hostTextField.text;
	NSString *username = _usernameTextField.text;
	NSString *password = _passwordTextField.text;
	if (hostName.length == 0 ||
		username.length == 0 ||
		password.length == 0) {
		[UIAlertView showWithTitle:nil
						   message:NSLocalizedString(@"Please fill all fields", nil)
				 cancelButtonTitle:NSLocalizedString(@"OK", nil)
				 otherButtonTitles:nil
						  tapBlock:NULL];
		return;
	}
	
	SettingsManager *settings = [SettingsManager sharedManager];
	settings.username = username;
	settings.password = password;
	settings.hostName = hostName;
	
	XMPPConfiguration *configuration = [XMPPConfiguration new];
	configuration.hostName = hostName;
	
	XMPPTestUser *user = [XMPPTestUser new];
	user.username = [SettingsManager sharedManager].username;
	user.password = [SettingsManager sharedManager].password;
	configuration.user = user;
	
	[XMPPService sharedInstance].configuration = configuration;
	[[XMPPService sharedInstance] setupStream];
	[[XMPPService sharedInstance] signIn];
	
	[self dismissViewControllerAnimated:YES completion:NULL];
}

@end
