//
//  ChatsListTableViewController.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/21/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import <XMPPFrameworkArchiving/XMPPRoomCoreDataStorageOpenfire.h>
#import <XMPPFrameworkArchiving/XMPPService.h>
#import <XMPPFrameworkArchiving/XMPPChatViewController.h>

#import "UIAlertView+Blocks.h"
#import "SettingsManager.h"

#import "ChatsListTableViewController.h"

@interface ChatsListTableViewController ()

@property (nonatomic, weak) SettingsManager *settings;
@property (nonatomic, strong) NSArray *chats;

- (IBAction)joinToRoomButtonTapped:(id)sender;
- (IBAction)logoutButtonTapped:(id)sender;

@end

@implementation ChatsListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	_settings = [SettingsManager sharedManager];
	
	_chats = [_settings.chats allObjects];
	
	NSString *hostName = [SettingsManager sharedManager].hostName;
	if (hostName.length == 0 || [SettingsManager sharedManager].username.length == 0) {
		[self performSegueWithIdentifier:@"LoginViewController" sender:self];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.title = [SettingsManager sharedManager].hostName;
}

#pragma mark - Action methods

- (IBAction)joinToRoomButtonTapped:(id)sender {
	__weak typeof(self) weakSelf = self;
	UIAlertView *alert = [UIAlertView showWithTitle:nil
											message:NSLocalizedString(@"Create or Join to Room", nil)
											  style:UIAlertViewStylePlainTextInput
								  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
								  otherButtonTitles:@[NSLocalizedString(@"Join", nil)]
										   tapBlock:^(UIAlertView * __nonnull alertView, NSInteger buttonIndex) {
											   if (alertView.cancelButtonIndex != buttonIndex) {
												   [weakSelf createRoomWithId:[[alertView textFieldAtIndex:0] text]];
											   }
										   }];
	alert.shouldEnableFirstOtherButtonBlock = ^BOOL(UIAlertView *alertView) {
		return ([[[alertView textFieldAtIndex:0] text] length] > 0);
	};
}

- (IBAction)logoutButtonTapped:(id)sender {
	[SettingsManager sharedManager].username = nil;
	[SettingsManager sharedManager].password = nil;
	[SettingsManager sharedManager].chats = nil;
	[[XMPPService sharedInstance] teardownStream];
	
	// remove all chat room databases
	NSString *xmppPersistentStoreDirectory = [XMPPRoomCoreDataStorageOpenfire persistentStoreDirectory];
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:xmppPersistentStoreDirectory error:nil];
	NSArray *roomsFilenames = [files filteredArrayUsingPredicate:
							   [NSPredicate predicateWithFormat:@"self BEGINSWITH[cd] 'chatroom_'"]];
	for (NSString *filename in roomsFilenames) {
		[[NSFileManager defaultManager] removeItemAtPath:[xmppPersistentStoreDirectory stringByAppendingPathComponent:filename]
												   error:nil];
	}

	[self performSegueWithIdentifier:@"LoginViewController" sender:self];
}

#pragma mark - Properties methods

#pragma mark - Private methods

- (void)createRoomWithId:(NSString *)roomId {
	if (roomId.length == 0) {
		return;
	}
	_settings.chats = [_settings.chats setByAddingObject:roomId];
	_chats = [_settings.chats allObjects];
	[self.tableView reloadData];
	
	[self openChatWithRoomId:roomId];
}

- (void)openChatWithRoomId:(NSString *)roomId {
	if (roomId.length == 0) {
		return;
	}
	XMPPChatViewController *chatVC = [[XMPPChatViewController alloc] initWithNibName:nil bundle:nil];
	chatVC.roomId = roomId;
	UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:chatVC];
	[self presentViewController:navigation animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_chats count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatCellIdentifier"
															forIndexPath:indexPath];
	cell.textLabel.text = self.chats[indexPath.row];
    return cell;
}

#pragma mark - UITableView Delegated methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([XMPPService sharedInstance].isXmppConnected) {
		[self openChatWithRoomId:_chats[indexPath.row]];
	}
}

@end
