//
//  SettingsManager.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/21/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import "SettingsManager.h"

@implementation SettingsManager

+ (instancetype)sharedManager {
	static SettingsManager *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedMyManager = [[self alloc] init];
	});
	return sharedMyManager;
}

//Username
- (void)setUsername:(NSString *)username {
	[self saveUserValue:username forKey:@"username"];
}

- (NSString *)username {
	return [self userValueForKey:@"username"];
}

//Password
- (void)setPassword:(NSString *)password {
	[self saveUserValue:password forKey:@"password"];
}

- (NSString *)password {
	return [self userValueForKey:@"password"];
}

//HostName
- (void)setHostName:(NSString *)hostName {
	[self saveUserValue:hostName forKey:@"hostName"];
}

- (NSString *)hostName {
	return [self userValueForKey:@"hostName"];
}

- (void)setChats:(NSSet *)chats {
	[self saveUserValue:[NSKeyedArchiver archivedDataWithRootObject:chats] forKey:@"chats"];
}

- (NSSet *)chats {
	NSData *data = [self userValueForKey:@"chats"];
	return [NSKeyedUnarchiver unarchiveObjectWithData:data] ?: [NSSet set];
}

#pragma mark - Private methods

- (void)saveUserValue:(id)value forKey:(NSString *)key {
	[[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)userValueForKey:(NSString *)key {
	return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

@end
