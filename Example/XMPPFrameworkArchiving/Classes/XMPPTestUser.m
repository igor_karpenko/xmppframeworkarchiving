//
//  XMPPTestUser.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/22/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import "XMPPTestUser.h"

@implementation XMPPTestUser

// just for testing
- (NSString *)userId {
	if (!_userId) {
		NSString *userId = @"";
		NSInteger count = self.username.length;
		for (NSInteger i = 0; i < count; i++) {
			NSInteger index = [[self.username substringWithRange:NSMakeRange(i, 1)] characterAtIndex:0];
			userId = [userId stringByAppendingString:[@(index) stringValue]];
		}
		_userId = userId;
	}
	return _userId;
}

@end
