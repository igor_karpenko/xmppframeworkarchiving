//
//  XMPPTestUser.h
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/22/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import "XMPPUserProtocol.h"
#import <Foundation/Foundation.h>

@interface XMPPTestUser : NSObject <XMPPUserProtocol>

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;

@end
