//
//  SettingsManager.h
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 3/21/16.
//  Copyright © 2016 Igor Karpenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, strong) NSString *hostName;

@property (nonatomic, strong) NSSet *chats;

+ (instancetype)sharedManager;

@end
