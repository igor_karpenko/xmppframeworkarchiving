//
//  AppDelegate.h
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 03/17/2016.
//  Copyright (c) 2016 Igor Karpenko. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
