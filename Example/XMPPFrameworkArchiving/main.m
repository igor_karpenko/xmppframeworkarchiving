//
//  main.m
//  XMPPFrameworkArchiving
//
//  Created by Igor Karpenko on 03/23/2016.
//  Copyright (c) 2016 Igor Karpenko. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
