# XMPPFrameworkArchiving

[![CI Status](http://img.shields.io/travis/Igor Karpenko/XMPPFrameworkArchiving.svg?style=flat)](https://travis-ci.org/Igor Karpenko/XMPPFrameworkArchiving)
[![Version](https://img.shields.io/cocoapods/v/XMPPFrameworkArchiving.svg?style=flat)](http://cocoapods.org/pods/XMPPFrameworkArchiving)
[![License](https://img.shields.io/cocoapods/l/XMPPFrameworkArchiving.svg?style=flat)](http://cocoapods.org/pods/XMPPFrameworkArchiving)
[![Platform](https://img.shields.io/cocoapods/p/XMPPFrameworkArchiving.svg?style=flat)](http://cocoapods.org/pods/XMPPFrameworkArchiving)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XMPPFrameworkArchiving is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "XMPPFrameworkArchiving"
```

## Author

Igor Karpenko, i.karpenko@mozidev.com

## License

XMPPFrameworkArchiving is available under the MIT license. See the LICENSE file for more info.
