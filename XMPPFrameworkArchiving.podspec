#
# Be sure to run `pod lib lint XMPPFrameworkArchiving.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
	s.name             = "XMPPFrameworkArchiving"
	s.version          = "0.1.0"
	s.summary          = "XMPPFrameworkArchiving help to use xmpp on ios (Obj-C)."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  

	s.homepage         = "https://bitbucket.org/igor_karpenko/xmppframeworkarchiving"
	# s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
	s.license          = 'MIT'
	s.author           = { "Igor Karpenko" => "i.karpenko@mozidev.com" }
	s.source           = { :git => "https://igor_karpenko@bitbucket.org/igor_karpenko/xmppframeworkarchiving.git", :tag => s.version.to_s }
	# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

	s.platform     = :ios, '7.0'
	s.requires_arc = true

	s.source_files = 'Pod/Classes/**/*'
	s.resource_bundles = {
	'XMPPFrameworkArchiving' => ['Pod/Assets/*.png']
	}

	# s.public_header_files = 'Pod/Classes/**/*.h'
	s.frameworks = 'UIKit', 'Foundation', 'CoreData'
	s.dependency 'XMPPFramework', '~> 3.6'
	s.dependency 'Reachability', '~> 3.2'
	s.dependency 'MD5Digest', '~> 1.1'
	s.dependency 'EasyMapping', '~> 0.15'
	s.dependency 'CocoaLumberjack','~>1.9'
	s.dependency 'JSQMessagesViewController', '~> 7.2'
	s.dependency 'JSQSystemSoundPlayer', '~> 2.0.1'

	s.xcconfig = {
	'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2 $(SDKROOT)/usr/include/libresolv $(PODS_ROOT)/XMPPFramework/module',
	'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
	'OTHER_LDFLAGS' => '"$(inherited)" "-lxml2" "-objc"',
	'ENABLE_BITCODE' => 'NO',
	}
end
