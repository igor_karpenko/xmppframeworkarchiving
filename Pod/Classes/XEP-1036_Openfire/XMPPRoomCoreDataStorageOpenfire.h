//
//  XMPPRoomCoreDataStorageOpenfire.h
//  PlsPlsMe
//
//  Created by Igor Karpenko on 2/12/16.
//  Copyright © 2016 DigiCode. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>
#import "XMPPRoomCoreDataStorage.h"

@interface XMPPRoomCoreDataStorageOpenfire : XMPPRoomCoreDataStorage


/**
 * Returns the timestamp of the oldest message stored in the database for the given room.
 * This may be used when requesting the message history from the server,
 * to prevent redownloading messages you already have.
 *
 * @param roomJID    - The JID of the room (a bare JID)
 *
 * @param xmppStream - This class can support multiple concurrent xmppStreams.
 *                     Optionally pass the xmppStream the room applies to.
 *                     If you're using this claass with a single xmppStream, you can pass nil.
 *
 * @param moc        - The managedObjectContext to use when doing the lookups.
 *                     If non-nil, this should match the thread you're currently using.
 *                     If nil, the operation is dispatch_sync'd onto the internal queue,
 *                     and uses the internal managedObjectContext.
 *
 * The moc may optionally be nil strictly because this method does not return a NSManagedObject.
 **/

+ (NSString *)persistentStoreDirectory;

- (NSDate *)mostOldestMessageTimestampForRoom:(XMPPJID *)roomJID
									   stream:(XMPPStream *)xmppStream
									inContext:(NSManagedObjectContext *)inMoc;
@end
