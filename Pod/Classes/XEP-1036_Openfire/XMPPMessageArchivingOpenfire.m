//
//  XMPPMessageArchivingOpenfire.m
//  PlsPlsMe
//
//  Created by Igor Karpenko on 2/5/16.
//  Copyright © 2016 DigiCode. All rights reserved.
//

#import <EasyMapping/EasyMapping.h>
#import "ArchivedMessage.h"
#import "XMPPService.h"

#import "NSDate+XMPPDateTimeProfiles.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPRoomCoreDataStorageOpenfire.h"
#import "XMPPMessage+Additions.h"
#import "NSXMLElement+XEP_0203.h"

#import "XMPPMessageArchivingOpenfire.h"
#import "XMPPFramework.h"
#import "XMPPLogging.h"
#import "NSNumber+XMPP.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif


@interface NSDate(XMPPDateTimeProfilesPrivate)
- (NSString *)xmppStringWithDateFormat:(NSString *)dateFormat;
@end


@interface XMPPMessageArchivingOpenfire ()

@property (nonatomic, strong) id <XMPPUserProtocol> currentUser;
@property (nonatomic, strong) XMPPRoom *xmppRoom;
@property (nonatomic, assign) BOOL isLoadingEarliearMessages;

@property (nonatomic, assign) NSInteger totalMessages;

@end


@implementation XMPPMessageArchivingOpenfire

@synthesize roomCoreDataStorage;

#pragma mark - Interface methods

- (id)initWithRoomStorage:(XMPPRoomCoreDataStorageOpenfire *)storage xmppRoom:(XMPPRoom *)room {
	self = [super initWithMessageArchivingStorage:[XMPPMessageArchivingCoreDataStorage sharedInstance]];
	if (!self) {
		return nil;
	}
	self.clientSideMessageArchivingOnly = NO;
	self.xmppRoom = room;
	roomCoreDataStorage = storage;
	
	return self;
}

- (BOOL)hasEarliearMessages {
	return self.totalMessages > 0;
}

// old messages
- (void)checkEarlierMessages {
	[self loadEarliearMessagesWithCount:@(1)];
}

- (void)loadEarliearMessages {
	[self loadEarliearMessagesWithCount:nil]; // if nil default value 25 on backend
}

// new messages if user didn't use app during long term we should load archived messages
- (void)checkAndLoadNewMessages {
	dispatch_async(dispatch_get_main_queue(), ^{
		[self loadNewMessages];
	});
}

- (void)loadMessagesFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate {
	dispatch_async(dispatch_get_main_queue(), ^{
		if (self.isLoadingEarliearMessages) {
			return;
		}
		self.isLoadingEarliearMessages = YES;
		dispatch_async(dispatch_get_main_queue(), ^{
			NSMutableDictionary *params = [@{@"xmpp_room_id" : [self.xmppRoom.roomJID user],
											 @"order_way" : @"DESC",
											 } mutableCopy];
			if (fromDate) {
				params[@"from_date"] = @([fromDate timeIntervalSince1970] * 1000.0);
			}
			if (toDate) {
				params[@"to_date"] = @([toDate timeIntervalSince1970] * 1000.0);
			}
			
			__weak typeof(self)weakSelf = self;
			if ([self.archivingDelegate respondsToSelector:@selector(getArchivedMessagesWithParameters:success:failure:)]) {
				[self.archivingDelegate getArchivedMessagesWithParameters:params success:^(id responseObject) {
					NSArray *messages = [self parseArchivedMessagesWithResponse:responseObject];
					NSInteger total = [self parseTotalCountWithResponse:responseObject];
					
					[weakSelf handleMessages:messages];
					
					if (total > messages.count) {
						NSDate *sentDate = ((ArchivedMessage *)[messages lastObject]).sentDate;
						[weakSelf loadMessagesFromDate:fromDate toDate:sentDate];
					} else {
						[weakSelf checkAndLoadNewMessages];
					}
					weakSelf.isLoadingEarliearMessages = NO;
				} failure:^(NSError *error) {
					weakSelf.isLoadingEarliearMessages = NO;
				}];
			}
		});
	});
}

#pragma mark - Properties methods

- (id<XMPPUserProtocol>)currentUser {
	if (!_currentUser) {
		_currentUser = [XMPPService sharedInstance].configuration.user;
	}
	return _currentUser;
}

#pragma mark - Private methods

- (void)handleMessages:(NSArray *)messages {
	if (messages.count == 0) {
		return;
	}
	messages = [[messages reverseObjectEnumerator] allObjects];
	
	NSString *currentUserId = [self.currentUser userId];
	NSString *currentUserName = [self.currentUser username];
	
	NSMutableArray *filtredMessages = [NSMutableArray array];
	
	for (ArchivedMessage *archivedMessage in messages) {
		NSString *body = archivedMessage.body;
		
		NSString *partnerId = archivedMessage.fromJID;
		NSString *partnerName = nil;
		
		if ([currentUserId isEqualToString:partnerId]) {
			partnerName = currentUserName;
			partnerId = currentUserId;
		} else {
			id <XMPPUserProtocol> partner = [self.archivingDelegate getUserWithId:partnerId];
			partnerName = [partner username];
		}
		XMPPMessage *message = [XMPPMessage messageWithType:@"groupchat" to:self.xmppStream.myJID];
		[message addAttributeWithName:@"xmlns" stringValue:@"jabber:client"];
		[message addAttributeWithName:@"from" stringValue:[[self.xmppRoom.roomJID description] stringByAppendingFormat:@"/%@", partnerName]];
		
		[message addChild:[DDXMLElement elementWithName:@"body" stringValue:body]];
		[message addChild:[DDXMLElement elementWithName:@"user_id" stringValue:partnerId]];
		[message addChild:[DDXMLElement elementWithName:@"username" stringValue:partnerName]];
		
		NSXMLElement *delay = [NSXMLElement elementWithName:@"delay" xmlns:@"urn:xmpp:delay"];
		NSString *dateString = [archivedMessage.sentDate xmppStringWithDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
		[delay addAttributeWithName:@"stamp" stringValue:dateString];
		
		[message addChild:delay];
		
		// check duplicates
		BOOL isContains = NO;
		for (XMPPMessage *cachedMessage in filtredMessages) {
			if ([[cachedMessage delayedDeliveryDate] isEqualToDate:[message delayedDeliveryDate]] &&
				[[cachedMessage user_id] isEqualToString:[message user_id]] &&
				[[cachedMessage body] isEqualToString:[message body]]) {
				isContains = YES;
				break;
			}
		}
		if (isContains) {
			continue;
		}
		[filtredMessages addObject:message];
		
		[self.roomCoreDataStorage handleIncomingMessage:(id)message room:self.xmppRoom];
	}
}

- (void)delegateFetchEarlearChats {
	dispatch_async(dispatch_get_main_queue(), ^{
		if ([self.archivingDelegate respondsToSelector:@selector(messageArchivingOpenfireDidFetchEarlearChats:)]) {
			[self.archivingDelegate messageArchivingOpenfireDidFetchEarlearChats:self];
		}
	});
}

- (void)removeChatWithDateString:(NSString *)dateString fromArray:(NSMutableArray *)array {
	for (NSXMLElement *chat in array) {
		if ([[chat attributeStringValueForName:@"start"] isEqualToString:dateString]) {
			[array removeObject:chat];
			break;
		}
	}
}

- (void)loadEarliearMessagesWithCount:(NSNumber *)count {
	if (self.isLoadingEarliearMessages) {
		return;
	}
	self.isLoadingEarliearMessages = YES;
	dispatch_async(dispatch_get_main_queue(), ^{
		NSMutableDictionary *params = [@{@"xmpp_room_id" : [self.xmppRoom.roomJID user],
										 @"order_way" : @"DESC",
										 } mutableCopy];
		if (count) {
			params[@"paginationCount"] = count;
		}
		
		NSDate *oldestMessageDate = [self.roomCoreDataStorage mostOldestMessageTimestampForRoom:self.xmppRoom.roomJID
																						 stream:self.xmppStream
																					  inContext:self.roomCoreDataStorage.mainThreadManagedObjectContext];
		if (oldestMessageDate) {
			params[@"to_date"] = @([oldestMessageDate timeIntervalSince1970] * 1000.0);
		} else if (count) {
			self.isLoadingEarliearMessages = NO;
			[self loadEarliearMessages];
			return;
		}
		
		__weak typeof(self)weakSelf = self;
		if ([self.archivingDelegate respondsToSelector:@selector(getArchivedMessagesWithParameters:success:failure:)]) {
			[self.archivingDelegate getArchivedMessagesWithParameters:params success:^(id responseObject) {
				NSArray *messages = [self parseArchivedMessagesWithResponse:responseObject];
				NSInteger total = [self parseTotalCountWithResponse:responseObject];
				ArchivedMessage *message = [messages lastObject];
				NSInteger messageTimeInterval = [@([message.sentDate timeIntervalSince1970]) integerValue];
				NSInteger oldestMessageTimeInterval = [@([oldestMessageDate timeIntervalSince1970]) integerValue];
				
				if (messages.count == 1 && total == 1 &&
					messageTimeInterval == oldestMessageTimeInterval) {
					weakSelf.totalMessages = 0;
				} else {
					weakSelf.totalMessages = total - messages.count;
					[weakSelf handleMessages:messages];
				}
				
				[weakSelf delegateFetchEarlearChats];
				weakSelf.isLoadingEarliearMessages = NO;
			} failure:^(NSError *error) {
				weakSelf.isLoadingEarliearMessages = NO;
			}];
		}
	});
}

- (void)loadNewMessages {
	dispatch_async(dispatch_get_main_queue(), ^{
		NSDate *mostRecentMessageDate = [self.roomCoreDataStorage mostRecentMessageTimestampForRoom:self.xmppRoom.roomJID
																							 stream:self.xmppStream
																						  inContext:self.roomCoreDataStorage.mainThreadManagedObjectContext];
		if (!mostRecentMessageDate) {
			[self checkEarlierMessages];
			return;
		}
		NSNumber *fromDate = @(([mostRecentMessageDate timeIntervalSince1970]) * 1000.0);
		
		NSMutableDictionary *params = [@{@"xmpp_room_id" : [self.xmppRoom.roomJID user],
										 @"from_date" : fromDate,
										 @"order_way" : @"ASC"
										 } mutableCopy];
		
		__weak typeof(self)weakSelf = self;
		if ([self.archivingDelegate respondsToSelector:@selector(getArchivedMessagesWithParameters:success:failure:)]) {
			[self.archivingDelegate getArchivedMessagesWithParameters:params success:^(id responseObject) {
				NSArray *messages = [self parseArchivedMessagesWithResponse:responseObject];
				NSInteger total = [self parseTotalCountWithResponse:responseObject];
				
				ArchivedMessage *message = [messages lastObject];
				NSInteger messageTimeInterval = [@([message.sentDate timeIntervalSince1970]) integerValue];
				NSInteger mostRecentMessageTimeInterval = [@([mostRecentMessageDate timeIntervalSince1970]) integerValue];
				
				if ((messages.count == 1 && total == 1 &&
					 messageTimeInterval == mostRecentMessageTimeInterval) == NO) {
					[weakSelf handleMessages:messages];
				}
				
				if (total > messages.count) {
					[weakSelf loadNewMessages];
				} else {
					[weakSelf checkEarlierMessages];
				}
			} failure:NULL];
		}
	});
}

// Parsing messages
- (NSArray *)parseArchivedMessagesWithResponse:(NSDictionary *)responseObject {
	return [EKMapper arrayOfObjectsFromExternalRepresentation:responseObject[@"messages"]
												  withMapping:[ArchivedMessage objectMapping]];
}

- (NSInteger)parseTotalCountWithResponse:(NSDictionary *)responseObject {
	return [responseObject[@"total"] integerValue];
}

@end
