//
//  XMPPMessageArchivingOpenfire.h
//  PlsPlsMe
//
//  Created by Igor Karpenko on 2/5/16.
//  Copyright © 2016 DigiCode. All rights reserved.
//

#import <XMPPFramework/XMPPFramework.h>
#import "XMPPMessageArchiving.h"
#import "XMPPRoom.h"
#import "XMPPUserProtocol.h"

@class XMPPRoomCoreDataStorageOpenfire, XMPPMessageArchivingOpenfire;

@protocol XMPPMessageArchivingOpenfireDelegate <NSObject>

@required
- (id <XMPPUserProtocol>)getUserWithId:(NSString *)userId;

@optional
- (void)messageArchivingOpenfireDidFetchEarlearChats:(XMPPMessageArchivingOpenfire *)archive;
- (void)getArchivedMessagesWithParameters:(NSDictionary *)parameters
								  success:(void (^)(id responseObject))success
								  failure:(void (^)(NSError *error))failure;


@end

@interface XMPPMessageArchivingOpenfire : XMPPMessageArchiving {
	__strong XMPPRoomCoreDataStorageOpenfire *roomCoreDataStorage;
}

@property (nonatomic, assign, readonly) BOOL hasEarliearMessages;
@property (readonly, strong) XMPPRoomCoreDataStorageOpenfire *roomCoreDataStorage;

@property (nonatomic, weak) id <XMPPMessageArchivingOpenfireDelegate> archivingDelegate;

- (id)initWithRoomStorage:(XMPPRoomCoreDataStorageOpenfire *)storage xmppRoom:(XMPPRoom *)room;


- (void)loadMessagesFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;

// old messages
- (void)checkEarlierMessages;
- (void)loadEarliearMessages;

// new messages if user didn't use app during long term we should load archived messages
- (void)checkAndLoadNewMessages;

@end
