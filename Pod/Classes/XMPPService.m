//
//  XMPPService.m
//  PlsPlsMe
//
//  Created by Igor Karpenko on 8/14/15.
//  Copyright (c) 2015 DigiCode. All rights reserved.
//

#import "XMPPService.h"

#import "XMPPConfiguration.h"

#import "XMPPFramework.h"
#import "GCDAsyncSocket.h"
#import "XMPP.h"
#import "XMPPReconnect.h"
#import "XMPPCapabilitiesCoreDataStorage.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMPPvCardAvatarModule.h"
#import "XMPPvCardCoreDataStorage.h"
#import <CFNetwork/CFNetwork.h>
#import "NSString+MD5.h"

#import "XMPPMessage+XEP0045.h"
#import "XMPPMUC.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPMessageArchivingOpenfire.h"
#import <Reachability/Reachability.h>

#import "DDLog.h"
#import "DDTTYLogger.h"
#import "XMPPLogging.h"

NSString *const XMPPStreamDidConectNotification = @"XMPPStreamDidConectNotification";
NSString *const XMPPStreamErrorNotification = @"XMPPStreamErrorNotification";

@interface XMPPService () <XMPPRosterDelegate, XMPPRoomDelegate, XMPPStreamDelegate> {
	BOOL isRegistering;
	BOOL isAuthenticating;
}

@property (nonatomic, strong) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong) XMPPRoster *xmppRoster;
@property (nonatomic, strong) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (nonatomic, strong) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, strong) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, strong) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (nonatomic, strong) XMPPvCardCoreDataStorage *xmppvCardStorage;

@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) BOOL customCertEvaluation;
@property (nonatomic, assign, readwrite) BOOL isXmppConnected;
@property (nonatomic, assign) BOOL isFirstPresence;

- (void)goOnline;
- (void)goOffline;

@end

@implementation XMPPService

#pragma mark - Static methods

+ (instancetype)sharedInstance {
	static XMPPService *sharedService = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedService = [[self alloc] init];
	});
	return sharedService;
}

#pragma mark - Interface methods

- (instancetype)init {
	self = [super init];
	if (!self) {
		return nil;
	}
	
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	[reachability startNotifier];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(reachabilityChangedNotification:)
												 name:kReachabilityChangedNotification
											   object:reachability];
	
	return self;
}

- (void)reachabilityChangedNotification:(NSNotification *)notification {
	Reachability *reachability =  notification.object;
	if ([reachability currentReachabilityStatus] != NotReachable) {
		[self signIn];
	}
}

#pragma mark - Private methods

- (NSString *)myJIDWithUserId:(NSString *)userId {
	return [userId stringByAppendingFormat:@"@%@", self.configuration.hostName];
}

- (NSString *)xmppPasswordWithUserId:(NSString *)userId {
	return [userId MD5Digest];
}

- (void)setupStream {
	NSAssert(_configuration != nil, @"configuration property shouldn't be nil!!!");
	NSAssert(_xmppStream == nil, @"Method setupStream invoked multiple times");
	
	// Setup xmpp stream
	//
	// The XMPPStream is the base class for all activity.
	// Everything else plugs into the xmppStream, such as modules/extensions and delegates.
	
	_xmppStream = [[XMPPStream alloc] init];
	
#if !TARGET_IPHONE_SIMULATOR
	{
		// Want xmpp to run in the background?
		//
		// P.S. - The simulator doesn't support backgrounding yet.
		//        When you try to set the associated property on the simulator, it simply fails.
		//        And when you background an app on the simulator,
		//        it just queues network traffic til the app is foregrounded again.
		//        We are patiently waiting for a fix from Apple.
		//        If you do enableBackgroundingOnSocket on the simulator,
		//        you will simply see an error message from the xmpp stack when it fails to set the property.
		
		_xmppStream.enableBackgroundingOnSocket = YES;
	}
#endif
	
	// Setup reconnect
	//
	// The XMPPReconnect module monitors for "accidental disconnections" and
	// automatically reconnects the stream for you.
	// There's a bunch more information in the XMPPReconnect header file.
	
	_xmppReconnect = [[XMPPReconnect alloc] init];
	
	// Setup roster
	//
	// The XMPPRoster handles the xmpp protocol stuff related to the roster.
	// The storage for the roster is abstracted.
	// So you can use any storage mechanism you want.
	// You can store it all in memory, or use core data and store it on disk, or use core data with an in-memory store,
	// or setup your own using raw SQLite, or create your own storage mechanism.
	// You can do it however you like! It's your application.
	// But you do need to provide the roster with some storage facility.
	
	_xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
	//	xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithInMemoryStore];
	
	_xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:_xmppRosterStorage];
	
	_xmppRoster.autoFetchRoster = YES;
	_xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
	
	// Setup vCard support
	//
	// The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
	// The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
	
	_xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
	_xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:_xmppvCardStorage];
	
	_xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:_xmppvCardTempModule];
	
	// Setup capabilities
	//
	// The XMPPCapabilities module handles all the complex hashing of the caps protocol (XEP-0115).
	// Basically, when other clients broadcast their presence on the network
	// they include information about what capabilities their client supports (audio, video, file transfer, etc).
	// But as you can imagine, this list starts to get pretty big.
	// This is where the hashing stuff comes into play.
	// Most people running the same version of the same client are going to have the same list of capabilities.
	// So the protocol defines a standardized way to hash the list of capabilities.
	// Clients then broadcast the tiny hash instead of the big list.
	// The XMPPCapabilities protocol automatically handles figuring out what these hashes mean,
	// and also persistently storing the hashes so lookups aren't needed in the future.
	//
	// Similarly to the roster, the storage of the module is abstracted.
	// You are strongly encouraged to persist caps information across sessions.
	//
	// The XMPPCapabilitiesCoreDataStorage is an ideal solution.
	// It can also be shared amongst multiple streams to further reduce hash lookups.
	
	_xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
	_xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:_xmppCapabilitiesStorage];
	
	_xmppCapabilities.autoFetchHashedCapabilities = YES;
	_xmppCapabilities.autoFetchNonHashedCapabilities = NO;

	// Activate xmpp modules
	
	[_xmppReconnect         activate:_xmppStream];
	[_xmppRoster            activate:_xmppStream];
	[_xmppvCardTempModule   activate:_xmppStream];
	[_xmppvCardAvatarModule activate:_xmppStream];
	[_xmppCapabilities      activate:_xmppStream];
	
	// Add ourself as a delegate to anything we may be interested in
	
	[_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
	[_xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
	
	// Optional:
	//
	// Replace me with the proper domain and port.
	// The example below is setup for a typical google talk account.
	//
	// If you don't supply a hostName, then it will be automatically resolved using the JID (below).
	// For example, if you supply a JID like 'user@quack.com/rsrc'
	// then the xmpp framework will follow the xmpp specification, and do a SRV lookup for quack.com.
	//
	// If you don't specify a hostPort, then the default (5222) will be used.
	
	[_xmppStream setHostName:self.configuration.hostName];
	[_xmppStream setHostPort:self.configuration.hostPort];
	
	// You may need to alter these settings depending on the server you're connecting to
	_customCertEvaluation = YES;
}

- (void)teardownStream {
	[_xmppStream removeDelegate:self];
	[_xmppRoster removeDelegate:self];
	
	[_xmppReconnect         deactivate];
	[_xmppRoster            deactivate];
	[_xmppvCardTempModule   deactivate];
	[_xmppvCardAvatarModule deactivate];
	[_xmppCapabilities      deactivate];
	
	[_xmppStream disconnect];
	
	self.xmppStream = nil;
	self.xmppReconnect = nil;
	self.xmppRoster = nil;
	self.xmppRosterStorage = nil;
	self.xmppvCardStorage = nil;
	self.xmppvCardTempModule = nil;
	self.xmppvCardAvatarModule = nil;
	self.xmppCapabilities = nil;
	self.xmppCapabilitiesStorage = nil;
}

- (void)goOnline {
	XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
	[[self xmppStream] sendElement:presence];
}

- (void)goOffline {
	XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
	
	[[self xmppStream] sendElement:presence];
}

- (void)logError:(NSError *)error {
	if (!error) {
		return;
	}
	[[NSNotificationCenter defaultCenter] postNotificationName:XMPPStreamErrorNotification
														object:nil userInfo:@{@"error" : error}];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Connect/disconnect
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)signIn {
	if (![_xmppStream isDisconnected] || !self.configuration.user) {
		return;
	}
	NSString *userId = [self.configuration.user userId];
	NSString *myJID = [self myJIDWithUserId:userId];
	NSString *myPassword = [self.configuration.user password];
	if (!myPassword) {
		myPassword = [self xmppPasswordWithUserId:userId];
	}
	
	if (myJID == nil || myPassword == nil) {
		return;
	}
	
	[_xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
	_password = myPassword;
	
	NSError *error = nil;
	if (![_xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error]) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
															message:@"See console for error details."
														   delegate:nil
												  cancelButtonTitle:@"Ok"
												  otherButtonTitles:nil];
		[alertView show];
		
		DDLogError(@"Error connecting: %@", error);
	}
}

- (void)signUp {
	if (![_xmppStream isDisconnected] || !self.configuration.user) {
		return;
	}
	
	NSString *userId = [self.configuration.user userId];
	NSString *myJID = [self myJIDWithUserId:userId];
	NSString *myPassword = [self.configuration.user password];
	if (!myPassword) {
		myPassword = [self xmppPasswordWithUserId:userId];
	}
	if (myJID == nil || myPassword == nil) {
		return;
	}
	
	[_xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
	_password = myPassword;
	
	NSError *error = nil;
	BOOL success;
	
	if (![[self xmppStream] isConnected]) {
		success = [[self xmppStream] connectWithTimeout:XMPPStreamTimeoutNone error:&error];
	} else {
		success = [[self xmppStream] registerWithPassword:_password error:&error];
	}
	
	if (success) {
		isRegistering = YES;
	} else {
		[self logError:error];
	}
}

- (void)disconnect {
	[self goOffline];
	[_xmppStream disconnect];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	NSString *expectedCertName = [_xmppStream.myJID domain];
	if (expectedCertName)
	{
		[settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
	}
	
	if (_customCertEvaluation) {
		[settings setObject:@(YES) forKey:@"GCDAsyncSocketManuallyEvaluateTrust"];
	}
}

- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust
 completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	// The delegate method should likely have code similar to this,
	// but will presumably perform some extra security code stuff.
	// For example, allowing a specific self-signed certificate that is known to the app.
	
	dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(bgQueue, ^{
		
		SecTrustResultType result = kSecTrustResultDeny;
		OSStatus status = SecTrustEvaluate(trust, &result);
		
		if (status == noErr && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified)) {
			completionHandler(YES);
		}
		else {
			completionHandler(NO);
		}
	});
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	_isXmppConnected = YES;
	
	NSError *error = nil;
	BOOL operationInProgress;
	
	if (isRegistering) {
		operationInProgress = [[self xmppStream] registerWithPassword:_password error:&error];
	} else {
		operationInProgress = [[self xmppStream] authenticateWithPassword:_password error:&error];
	}
	
	if (!operationInProgress) {
		[self logError:error];
	}
}

- (void)xmppStreamDidRegister:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	// Update tracking variables
	isRegistering = NO;
	
	//	[SVProgressHUD showSuccessWithStatus:@"Registered new user"];
	NSString *userId = [self.configuration.user userId];
	NSString *myPassword = [self.configuration.user password];
	if (!myPassword) {
		myPassword = [self xmppPasswordWithUserId:userId];
	}
	[sender authenticateWithPassword:myPassword error:nil];
}

- (void)xmppStream:(XMPPStream *)sender didNotRegister:(NSXMLElement *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	// Update tracking variables
	isRegistering = NO;
	
	// Update GUI
	//	[SVProgressHUD showErrorWithStatus:@"Sorry, username is already taken"];
	[sender disconnect];
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	_isFirstPresence = YES;
	[self goOnline];
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	[_xmppStream disconnect];
	[self signUp];
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	NSLog(@"%@", iq);
	
	return NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
	DDLogVerbose(@"%@: %@ - %@", THIS_FILE, THIS_METHOD, [presence fromStr]);
	
	if (_isFirstPresence) {
		_isFirstPresence = NO;
		
		[[NSNotificationCenter defaultCenter] postNotificationName:XMPPStreamDidConectNotification
															object:nil];
	}
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	
	if (!_isXmppConnected) {
		[self logError:error];
		DDLogError(@"Unable to connect to server. Check xmppStream.hostName");
	}
	
	DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
	isAuthenticating = NO;
	_isXmppConnected = NO;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRosterDelegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppRoster:(XMPPRoster *)sender didReceiveBuddyRequest:(XMPPPresence *)presence {
	
}

@end
