//
//  XMPPService.h
//  PlsPlsMe
//
//  Created by Igor Karpenko on 8/14/15.
//  Copyright (c) 2015 DigiCode. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DDLog.h"
#import "XMPPConfiguration.h"
#import "XMPPMessage.h"

// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif

@class XMPPRoom, XMPPStream, XMPPConfiguration;

@interface XMPPService : NSObject

@property (nonatomic, strong) XMPPStream *xmppStream;
@property (nonatomic, strong) XMPPConfiguration *configuration;

@property (nonatomic, assign, readonly) BOOL isXmppConnected;

+ (instancetype)sharedInstance;

- (void)signIn;
- (void)signUp;
- (void)disconnect;

- (void)setupStream;
- (void)teardownStream;

@end

extern NSString *const XMPPStreamDidConectNotification;
extern NSString *const XMPPStreamErrorNotification;
