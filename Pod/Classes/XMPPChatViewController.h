//
//  XMPPChatViewController.h
//  Pods
//
//  Created by Igor Karpenko on 3/23/16.
//
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <CoreData/CoreData.h>
#import "XMPPMUC.h"

@class XMPPChatViewController;

@protocol XMPPChatViewControllerDelegate <NSObject>
- (void)xmppChatViewController:(XMPPChatViewController *)controller
		  didUpdateLastMessage:(NSString *)message
						userId:(NSString *)userId
					   history:(NSDictionary *)history;
@end


@interface XMPPChatViewController : JSQMessagesViewController <XMPPRoomDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSString *roomId;
@property (nonatomic, assign) BOOL serverArchivingEnabled; // default value is NO
@property (weak, nonatomic) id <XMPPChatViewControllerDelegate> delegate;

// override for showing Progress HUD, need call [super initXmppChatRoom]
- (void)initXmppChatRoom;

// override for hidding Progress HUD , need call [super checkRoomConnection]
- (void)checkRoomConnection;

// override for hidding Progress HUD , need call [super finishReceivingMessageNotAnimated]
- (void)finishReceivingMessageNotAnimated;

// you can override for customization
- (void)configureChatUI;

// override for showing Progress HUD , need call [super applicationDidBecomeActive]
- (void)applicationDidBecomeActive:(id)sender;

/*
 // override this XMPPRoomDelegate methods for hiding Progress HUD , need call [super ...]
 - (void)xmppRoomDidCreate:(XMPPRoom *)sender;
 - (void)xmppRoomDidLeave:(XMPPRoom *)sender
 */

@end
