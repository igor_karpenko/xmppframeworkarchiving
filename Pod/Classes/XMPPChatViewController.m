////
////  XMPPChatViewController.m
////  Pods
////
////  Created by Igor Karpenko on 3/23/16.
////
////


#import <JSQMessagesViewController/JSQMessages.h>
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>

#import "XMPPMessageArchivingOpenfire.h"
#import "XMPPRoomCoreDataStorageOpenfire.h"
#import "XMPPService.h"
#import "XMPPMessage+Additions.h"

#import "NSDate+XMPPDateTimeProfiles.h"
#import "XMPPMessage+XEP0045.h"
#import "NSXMLElement+XEP_0203.h"

#import "XMPPChatViewController.h"

@interface XMPPChatViewController () <XMPPMessageArchivingOpenfireDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) XMPPService *xmppService;
@property (strong, nonatomic) XMPPRoom *xmppRoom;
@property (strong, nonatomic) XMPPMessageArchivingOpenfire *messageArchivingModule;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (nonatomic, strong) NSDate *localLastMessageDate;
@property (nonatomic, strong) NSDate *serverFirstMessageDate;

@property (nonatomic, assign) BOOL shouldScrollToBottom;

@end

@implementation XMPPChatViewController

#pragma mark - Interface methods

- (void)dealloc {
	[self clearChatRoom];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = self.roomId;
	
	[self configureChatUI];
	[self addNotificationObserver];
	[self initXmppChatRoom];
	
	UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil)
																	style:UIBarButtonItemStylePlain
																   target:self
																   action:@selector(closeButtonTapped:)];
	self.navigationItem.leftBarButtonItem = closeButton;
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
																				 action:@selector(viewTapped:)];
	tapGesture.delegate = self;
	[self.collectionView addGestureRecognizer:tapGesture];
}

- (void)initXmppChatRoom {
	self.xmppService = [XMPPService sharedInstance];
	
	self.senderId = [self.xmppService.configuration.user userId];
	self.senderDisplayName = [self.xmppService.configuration.user username];
	
	[self connectToRoom];
}

- (void)checkRoomConnection {
	if (self.xmppRoom && self.xmppRoom.isJoined == NO) {
		[self.xmppRoom leaveRoom];
		
		[self closeButtonTapped:nil];
	}
}

- (void)finishReceivingMessageNotAnimated {
	[self finishReceivingMessageAnimated:NO];
	
	if (self.localLastMessageDate && self.serverFirstMessageDate) {
		[self.messageArchivingModule loadMessagesFromDate:self.localLastMessageDate toDate:self.serverFirstMessageDate];
	} else {
		[self.messageArchivingModule checkAndLoadNewMessages];
	}
}


- (void)configureChatUI {
	JSQMessagesBubbleImageFactory *bubbleFactory = [JSQMessagesBubbleImageFactory new];
	self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:15.0f/255.0f
																									   green:134.0f/255.0f
																										blue:254.0f/255.0f
																									   alpha:1.0]];
	self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:230.0f/255.0f
																									   green:230.0f/255.0f
																										blue:235.0f/255.0f
																									   alpha:1.0]];
	self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
	self.inputToolbar.contentView.leftBarButtonItem = nil;
}

#pragma mark - Properties methods

- (NSFetchedResultsController *)fetchedResultsController {
	if (_fetchedResultsController) return _fetchedResultsController;
	
	if (![self xmppStorage].mainThreadManagedObjectContext) return nil;
	
	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	NSEntityDescription *entityDesc = [[self xmppStorage] messageEntity:[self xmppStorage].mainThreadManagedObjectContext];
	[fetchRequest setEntity:entityDesc];
	fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"localTimestamp" ascending:YES]];
	
	_fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
																	managedObjectContext:[self xmppStorage].mainThreadManagedObjectContext
																	  sectionNameKeyPath:nil
																			   cacheName:nil];
	_fetchedResultsController.delegate = self;
	
	NSError *error = nil;
	if (![_fetchedResultsController performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	return _fetchedResultsController;
}

- (XMPPRoomCoreDataStorageOpenfire *)xmppStorage {
	return self.xmppRoom.xmppRoomStorage;
}

#pragma mark - Action methods

- (void)closeButtonTapped:(id)sender {
	[self clearChatRoom];
	if (self.navigationController.presentingViewController != nil) {
		[self dismissViewControllerAnimated:YES completion:NULL];
	} else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)viewTapped:(id)sender {
	if (self.keyboardController.keyboardIsVisible) {
		[self.inputToolbar.contentView.textView resignFirstResponder];
	}
}

#pragma mark - Private methods

- (void)reConnectChat {
	if (self.xmppRoom && self.xmppRoom.isJoined == NO) {
		[self.xmppRoom leaveRoom];
		[self connectToRoom];
	}
}

- (void)scheduleCheckRoomConnection {
	[self cancelCheckRoomConnection];
	[self performSelector:@selector(checkRoomConnection) withObject:nil afterDelay:15.0f];
}

- (void)cancelCheckRoomConnection {
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkRoomConnection) object:nil];
}

- (void)clearChatRoom {
	[self.view endEditing:YES];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[self cancelCheckRoomConnection];
	
	self.collectionView.delegate = nil;
	[self.xmppRoom deactivate];
	[self.xmppRoom leaveRoom];
	[self.xmppRoom removeDelegate:self delegateQueue:dispatch_get_main_queue()];
	
	[self.messageArchivingModule deactivate];
	[self.messageArchivingModule removeDelegate:self delegateQueue:dispatch_get_main_queue()];
	self.messageArchivingModule.archivingDelegate = nil;
	
	self.xmppRoom = nil;
	self.roomId = nil;
	self.xmppService = nil;
}

- (void)addNotificationObserver {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(streamDidConect:)
												 name:XMPPStreamDidConectNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appWillResignActive:)
												 name:UIApplicationWillResignActiveNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(applicationDidBecomeActive:)
												 name:UIApplicationDidBecomeActiveNotification object:nil];
	
}

- (void)connectToRoom {
	if (self.roomId) {
		if (!_xmppRoom) {
			// saving chat history in different databases
			NSString *fileName = [@"chatroom_" stringByAppendingString:self.roomId];
			XMPPRoomCoreDataStorageOpenfire *xmppRoomCoreDataStorage = (id)[[XMPPRoomCoreDataStorageOpenfire alloc] initWithDatabaseFilename:fileName
																																storeOptions:nil];
			xmppRoomCoreDataStorage.maxMessageAge = 0;
			xmppRoomCoreDataStorage.deleteInterval = 0;
			
			XMPPJID * roomJID = [XMPPJID jidWithString:[self.roomId stringByAppendingFormat:@"%@", self.xmppService.configuration.conferenceHostName]];
			_xmppRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppRoomCoreDataStorage jid:roomJID];
			[_xmppRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
			
			if (self.serverArchivingEnabled) {
				_messageArchivingModule = [[XMPPMessageArchivingOpenfire alloc] initWithRoomStorage:xmppRoomCoreDataStorage xmppRoom:_xmppRoom];
				[_messageArchivingModule activate:self.xmppService.xmppStream];
				[_messageArchivingModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
				_messageArchivingModule.archivingDelegate = self;
			}
		}
		[self createOrJoinToRoom:_xmppRoom];
	}
}

- (void)createOrJoinToRoom:(XMPPRoom *)room {
	[room activate:_xmppService.xmppStream];
	
	NSDate *lastMessageDate = [[self xmppStorage] mostRecentMessageTimestampForRoom:_xmppRoom.roomJID
																			 stream:_xmppService.xmppStream
																		  inContext:[self xmppStorage].mainThreadManagedObjectContext];
	NSString *stringDate = @"1970-01-01T00:00:00Z";
	if (lastMessageDate) {
		lastMessageDate = [lastMessageDate dateByAddingTimeInterval:-1.0f];
		stringDate = [lastMessageDate xmppDateTimeString];
		
		self.localLastMessageDate = lastMessageDate;
	}
	
	NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
	[history addAttributeWithName:@"since" stringValue:stringDate];
	[room joinRoomUsingNickname:self.senderDisplayName
						history:history
					   password:nil];
	
	[self scheduleCheckRoomConnection];
}

- (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
	NSCalendar* calendar = [NSCalendar currentCalendar];
	
	unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
	NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
	NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
	
	return [comp1 day]   == [comp2 day] &&
	[comp1 month] == [comp2 month] &&
	[comp1 year]  == [comp2 year];
}

- (BOOL)shouldShowDateAtIndexPath:(NSIndexPath *)indexPath {
	XMPPRoomMessageCoreDataStorageObject *messageObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	if (indexPath.item - 1 >= 0) {
		indexPath = [NSIndexPath indexPathForItem:indexPath.item - 1 inSection:indexPath.section];
		XMPPRoomMessageCoreDataStorageObject *previousMessageObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
		if ([self isSameDayWithDate1:messageObject.localTimestamp date2:previousMessageObject.localTimestamp] == NO) {
			return YES;
		}
	} else if(indexPath.item == 0) {
		return YES;
	}
	return NO;
}

- (JSQMessage *)messageAtIndexPath:(NSIndexPath *)indexPath {
	XMPPRoomMessageCoreDataStorageObject *messageObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	XMPPMessage *message = messageObject.message;
	NSDate *date = [messageObject localTimestamp];
	if (!date) {
		date = [NSDate date];
	}
	JSQMessage *jsqMessage = [[JSQMessage alloc] initWithSenderId:message.user_id
												senderDisplayName:message.username
															 date:date
															 text:message.body];
	return jsqMessage;
}

- (void)reloadCollectionView {
	self.showTypingIndicator = NO;
	[self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
	[self.collectionView reloadData];
}

#pragma mark - Notification observers

- (void)streamDidConect:(NSNotification *)notification {
	[self connectToRoom];
}

- (void)appWillResignActive:(NSNotification*)notification {
	[self.view endEditing:YES];
	
	[self.xmppRoom deactivate];
}

- (void)applicationDidBecomeActive:(id)sender {
	[self reConnectChat];
}

#pragma mark - JSQMessagesViewController method overrides
- (void)didPressSendButton:(UIButton *)button
		   withMessageText:(NSString *)text
				  senderId:(NSString *)senderId
		 senderDisplayName:(NSString *)senderDisplayName
					  date:(NSDate *)date {
	
	[JSQSystemSoundPlayer jsq_playMessageSentSound];
	
	XMPPMessage *message = [XMPPMessage messageWithType:@"groupchat" to:_xmppRoom.roomJID];
	[message addBody:text];
	message.user_id = self.senderId;
	message.username = self.senderDisplayName;
	
	[self.xmppRoom sendMessage:message];
	
	[self finishSendingMessageAnimated:YES];
	self.inputToolbar.contentView.rightBarButtonItem.userInteractionEnabled = NO;
	
	__weak typeof(self)weakSelf = self;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		weakSelf.inputToolbar.contentView.rightBarButtonItem.userInteractionEnabled = YES;
	});
}

#pragma mark - JSQMessages CollectionView Delegate

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
				header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
	[self.messageArchivingModule loadEarliearMessages];
}

#pragma mark - JSQMessages CollectionView DataSource
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
	JSQMessage *message = [self messageAtIndexPath:indexPath];
	return message;
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
	JSQMessage *message = [self messageAtIndexPath:indexPath];
	if ([message.senderId isEqualToString:self.senderId]) {
		return self.outgoingBubbleImageData;
	}
	return self.incomingBubbleImageData;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
	if ([self shouldShowDateAtIndexPath:indexPath]) {
		JSQMessage *message = [self messageAtIndexPath:indexPath];
		return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
	}
	return nil;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
	if ([self shouldShowDateAtIndexPath:indexPath]) {
		return kJSQMessagesCollectionViewCellLabelHeightDefault;
	}
	return 0.0f;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
	XMPPRoomMessageCoreDataStorageObject *messageObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	XMPPMessage *message = messageObject.message;
	
	return [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[[message.username substringToIndex:MIN(2, message.username.length)] uppercaseString]
													  backgroundColor:[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:235.0f/255.0f alpha:1.0f]
															textColor:[UIColor lightGrayColor]
																 font:[UIFont systemFontOfSize:20.0f]
															 diameter:50.0f];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return [self.fetchedResultsController.fetchedObjects count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
	
	JSQMessage *message = [self messageAtIndexPath:indexPath];
	cell.textView.textColor = [message.senderId isEqualToString:self.senderId] ? [UIColor whiteColor] : [UIColor blackColor];
	cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
										  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
	return cell;
}

#pragma mark - XMPPRoom Delegated methods

- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
	[sender fetchConfigurationForm];
	[self cancelCheckRoomConnection];
}

- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
	[self cancelCheckRoomConnection];
	[self performSelector:@selector(finishReceivingMessageNotAnimated) withObject:nil afterDelay:2.0f];
}

- (void)xmppRoom:(XMPPRoom *)sender didFetchConfigurationForm:(NSXMLElement *)configForm {
	NSXMLElement *newConfig = [configForm copy];
	NSArray *fields = [newConfig elementsForName:@"field"];
	
	for (NSXMLElement *field in fields) {
		NSString *var = [field attributeStringValueForName:@"var"];
		if ([var isEqualToString:@"muc#roomconfig_persistentroom"]) {
			[field removeChildAtIndex:0];
			[field addChild:[NSXMLElement elementWithName:@"value" stringValue:@"1"]];
		}
	}
	[sender configureRoomUsingOptions:newConfig];
}

- (void)xmppRoom:(XMPPRoom *)sender didConfigure:(XMPPIQ *)iqResult {
}

- (void)xmppRoom:(XMPPRoom *)sender didNotConfigure:(XMPPIQ *)iqResult {
}

- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID {
	if (message.wasDelayed) {
		if (!self.serverFirstMessageDate && ![self.localLastMessageDate isEqualToDate:message.delayedDeliveryDate]) {
			self.serverFirstMessageDate = message.delayedDeliveryDate;
		}
		[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishReceivingMessageNotAnimated) object:nil];
		[self performSelector:@selector(finishReceivingMessageNotAnimated) withObject:nil afterDelay:1.0f];
	}
}

#pragma mark - UIGestureRecognizer Delegated methods
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return YES;
}

#pragma mark - NSFetchedResultsController Delegated methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	self.shouldScrollToBottom = NO;
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
	   atIndexPath:(nullable NSIndexPath *)indexPath
	 forChangeType:(NSFetchedResultsChangeType)type
	  newIndexPath:(nullable NSIndexPath *)newIndexPath {
	
	self.shouldScrollToBottom = MAX(self.shouldScrollToBottom,
									newIndexPath.item == self.fetchedResultsController.fetchedObjects.count - 1);
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	if (self.shouldScrollToBottom) {
		[self finishReceivingMessage];
	} else {
		[self reloadCollectionView];
	}
}

#pragma mark - XMPPMessageArchivingOpenfire Delegated methods

- (id <XMPPUserProtocol>)getUserWithId:(NSString *)userId {
	return nil;
}

- (void)messageArchivingOpenfireDidFetchEarlearChats:(XMPPMessageArchivingOpenfire *)archive {
	BOOL previousValue = self.showLoadEarlierMessagesHeader;
	self.showLoadEarlierMessagesHeader = [archive hasEarliearMessages];
	
	if (previousValue == NO && self.showLoadEarlierMessagesHeader == YES) {
		CGPoint point = self.collectionView.contentOffset;
		point.y += kJSQMessagesLoadEarlierHeaderViewHeight;
		self.collectionView.contentOffset = point;
	}
	[self finishReceivingMessage];
}

@end
