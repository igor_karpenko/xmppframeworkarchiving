//
//  XMPPConfiguration.h
//  Pods
//
//  Created by Igor Karpenko on 3/17/16.
//
//

#import "XMPPUserProtocol.h"
#import <Foundation/Foundation.h>

@interface XMPPConfiguration : NSObject

@property (nonatomic, strong) NSString *hostName;				//#define XMPP_HOST_NAME_STAGING @"api-stg.plsplsme.com"
@property (nonatomic, strong) NSString *conferenceHostName;		//Default value is @conference.<hostName>
@property (nonatomic, assign) NSInteger hostPort; // Default value is 5222

@property (nonatomic, strong) id <XMPPUserProtocol> user;

@end
