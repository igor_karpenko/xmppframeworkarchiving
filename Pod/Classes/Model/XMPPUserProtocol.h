//
//  XMPPUserProtocol.h
//  Pods
//
//  Created by Igor Karpenko on 3/21/16.
//
//

#import <Foundation/Foundation.h>

@protocol XMPPUserProtocol <NSObject>

@required
- (NSString *)userId;
- (NSString *)username;

@optional
- (NSString *)password;

@end
