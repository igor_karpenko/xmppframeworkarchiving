//
//  ArchivedMessage.h
//  PlsPlsMe
//
//  Created by Igor Karpenko on 2/18/16.
//  Copyright © 2016 DigiCode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@interface ArchivedMessage : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSString *fromJID;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSDate *sentDate;

@end
