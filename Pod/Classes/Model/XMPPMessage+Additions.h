//
//  XMPPMessage+Additions.h
//  PlsPlsMe
//
//  Created by Igor Karpenko on 8/14/15.
//  Copyright (c) 2015 DigiCode. All rights reserved.
//

#import "XMPPMessage.h"

@interface XMPPMessage (Additions)

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *username;

@end
