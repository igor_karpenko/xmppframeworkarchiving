//
//  XMPPConfiguration.m
//  Pods
//
//  Created by Igor Karpenko on 3/17/16.
//
//

#import "XMPPConfiguration.h"

@implementation XMPPConfiguration

- (instancetype)init {
	self = [super init];
	if (!self) {
		return nil;
	}
	self.hostPort = 5222;
	return self;
}

- (void)setHostName:(NSString *)hostName {
	_hostName = hostName;
	
	_conferenceHostName = [NSString stringWithFormat:@"@conference.%@", hostName];
}

@end
