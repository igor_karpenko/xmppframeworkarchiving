//
//  ArchivedMessage.m
//  PlsPlsMe
//
//  Created by Igor Karpenko on 2/18/16.
//  Copyright © 2016 DigiCode. All rights reserved.
//

#import <EasyMapping/NSDateFormatter+EasyMappingAdditions.h>

#import "ArchivedMessage.h"

@implementation ArchivedMessage

#pragma mark - Static methods

+ (EKObjectMapping *)objectMapping {
	return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
		[mapping mapPropertiesFromArray:@[@"fromJID", @"body"]];
		[mapping mapKeyPath:@"sentDate" toProperty:@"sentDate" withValueBlock:^id(NSString *key, NSNumber *value) {
			NSDate *date = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]/1000.0];
			return date;
		}];
	}];
}

@end
