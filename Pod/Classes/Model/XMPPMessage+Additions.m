//
//  XMPPMessage+Additions.m
//  PlsPlsMe
//
//  Created by Igor Karpenko on 8/14/15.
//  Copyright (c) 2015 DigiCode. All rights reserved.
//

#import "NSXMLElement+XMPP.h"

#import "XMPPMessage+Additions.h"

@implementation XMPPMessage (Additions)

#pragma mark - user_id
- (void)setUser_id:(NSString *)user_id {
	if (!user_id) {
		return;
	}
	[self removeElementForName:@"user_id"];
	NSXMLElement *userIdElement = [NSXMLElement elementWithName:@"user_id" stringValue:user_id];
	[self addChild:userIdElement];
}

- (NSString *)user_id {
	return [[self elementForName:@"user_id"] stringValue];
}

#pragma mark - username
- (void)setUsername:(NSString *)username {
	if (!username) {
		return;
	}
	[self removeElementForName:@"username"];
	NSXMLElement *userIdElement = [NSXMLElement elementWithName:@"username" stringValue:username];
	[self addChild:userIdElement];
}

- (NSString *)username {
	return [[self elementForName:@"username"] stringValue];
}

@end
